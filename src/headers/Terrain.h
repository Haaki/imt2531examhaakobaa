#pragma once

#include "GameObject.h"

class Terrain : public GameObject
{
public:
	bool changingSeasons = true;
	bool changingDaylight = true;
	float seasonGoal = 0;
	float dayGoal = 0;
	GameObject* water = nullptr;
	GameObject* forest = nullptr;
	std::vector<GameObject*> terrainSlices;

	Terrain();

	virtual void update();
	virtual void input(int key, int scancode, int action, int mods);
};