#include "GameObject.h"
#include "Globals.h"
class Glider : public GameObject
{
public:
	float turnSpeed = 2.0f;
	float speed = 0.05;
	float minSpeed, maxSpeed;

	Glider();

	void update();
	void input(int key, int scancode, int action, int mods);
};