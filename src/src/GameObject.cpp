#include "src/headers/GameObject.h"
#include "src/headers/mad.h"
#include "src/headers/Init.h"
#include "src/headers/Camera.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/Globals.h"

extern Camera* mainCamera;

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

GameObject::GameObject(glm::vec3 pos, glm::mat4 rot, glm::vec3 scale)
{
	transform.position = pos; // Position 0
	transform.rotation = rot; // Identity matrix
	transform.scale = scale; // Scale 1

	buffer = new Buffer;
}

void GameObject::init(Mesh* obj, Texture* tex, Shader* shade)
{
	gameObjects.push_back(this);

	mesh = new Mesh;

	material.shader = shade;

	/*mesh->normals.resize(obj.normals.size());
	mesh->texture_coordinates.resize(obj.texCoord.size());*/
	mesh = obj;
	prevMesh = mesh;

	//objloadertest(mesh, obj.c_str());

	material.texture = tex;

    if (mesh != nullptr) setMeshBuffers();
}

void GameObject::draw(glm::mat4 view, glm::mat4 projection)
{
	if (mesh != nullptr && material.shader != nullptr && material.texture != nullptr)
	{
		glm::mat4 model(1.0f);
		model = glm::translate(model, transform.position);
		model *= transform.rotation;
		model = glm::scale(model, transform.scale);

		material.shader->setVec3("La", lights[0]->color);
		material.shader->setVec3("Ld", lights[0]->color);
		material.shader->setVec3("Ls", lights[0]->color);

		material.shader->setVec3("Ka", glm::vec3(1));
		material.shader->setVec3("Ka", glm::vec3(1));
		material.shader->setVec3("Ka", glm::vec3(1));

		material.shader->setFloat("Shininess", material.specularity);

		material.shader->setMatrix("model", model);
		material.shader->setMatrix("view", view);
		material.shader->setMatrix("projection", projection);

		material.shader->setVec3("lightPos", lights[0]->transform.position);	//	GET FROM GLOBAL LIGHT
		material.shader->setVec3("lightColor", lights[0]->color);				//	GET FROM GLOBAL LIGHT
		material.shader->setVec3("camPos", mainCamera->transform.position);		//	GET FROM GLOBAL CAMERA
		material.shader->setVec4("vertexColor", material.color);
		material.shader->setVec4("matColor", material.color);
		material.shader->setFloat("specularity", material.specularity);

		if (mesh != prevMesh)
		{
			//std::cout << "Mesh updated\n";
			setMeshBuffers();
			prevMesh = mesh;
		}
		else
			glBindVertexArray(buffer->VAO);                             // Bind VAO for drawing
																		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->IBO);         // Bind IBO for drawing

		glBindTexture(GL_TEXTURE_2D, material.texture->getNo());
		glDrawArrays(GL_TRIANGLES, 0, mesh->vertices.size() * sizeof(glm::vec3));
	}
}


void GameObject::update()
{
    // To be implemented by child classes
}

GameObject::~GameObject()
{
	//	Do not delete mesh, mesh points to globally saved meshes
	//		Same goes for material.shader, and texture

	delete buffer;
}

void GameObject::input(int key, int scancode, int action, int mods)
{
	//	To be implemented by child classes
}

void GameObject::setMeshBuffers()
{
	glGenVertexArrays(1, &buffer->VAO);                                         // Generate VAO with unique ID
	glBindVertexArray(buffer->VAO);                                             // Binds the VAO so changed to buffers will be stored

	glGenBuffers(3, &buffer->VBO[0]);                                           // Generates 3 unique ID for buffer object
	glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[0]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here

	glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[0]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
	glBufferData(GL_ARRAY_BUFFER, (mesh->vertices.size() * sizeof(glm::vec3)), mesh->vertices.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
																				// Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

	glEnableVertexAttribArray(0); // <-------------------------------- Remember

	glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[1]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
	glBufferData(GL_ARRAY_BUFFER, (mesh->texCoord.size() * sizeof(glm::vec2)), mesh->texCoord.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
																				// Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[2]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
	glBufferData(GL_ARRAY_BUFFER, (mesh->normals.size() * sizeof(glm::vec3)), mesh->normals.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
																				// Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

	glEnableVertexAttribArray(2);
	/*
	glGenBuffers(1, &buffer->IBO);                                          // Generates unique ID for Element object
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->IBO);                     // Binds unique buffer to a target, GL_ELEMENT_ARRAY_BUFFER here
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->indices.size() * sizeof(glm::uvec3), mesh->indices.data(), GL_STATIC_DRAW);
	// Copies indices data into the GL_ELEMENT_ARRAY_BUFFER target
	*/
	// Unbinds everything
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}