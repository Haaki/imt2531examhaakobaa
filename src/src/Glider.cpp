#include "src/headers/Glider.h"
#include "src/headers/Globals.h"
#include <glm/glm/gtx/quaternion.hpp>

Glider::Glider()
{
	transform.position = home;
	transform.alignWith(glm::vec3(0, home.y, 0));
	minSpeed = speed / 10;
	maxSpeed = speed * 10;
}
void Glider::update()
{
	float ang = glm::acos(glm::dot(transform.forward(), glm::vec3(0, 1, 0)) / (glm::length(transform.forward()) * glm::length(glm::vec3(0, 1, 0)))) * 2;	// the difference between the plane's pitch and the up vector
	ang -= 1.0; // to simulate stalling	
	transform.position += transform.forward() * float(ang * speed * worldScale * g_deltatime);
}
void Glider::input(int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_R && action == GLFW_PRESS)
	{
		transform.position = home;
		speed = maxSpeed / 10;
		transform.alignWith(glm::vec3(0, home.y, 0));
	}
	if (key == GLFW_KEY_F && action == GLFW_PRESS)
	{
		int rX = rand() % 1024, rY = rand() % 1024;
		transform.position = glm::vec3((((rX / float(1024)) * 10.08f) - 5.04) * worldScale, home.y, ((rY / float(1024)) * 20.08f) - 10.04) * worldScale;
		speed = maxSpeed / 10;
		transform.alignWith(glm::vec3(0, home.y,0));
	}
	if (key == GLFW_KEY_W)
	{
		transform.pitch(turnSpeed * g_deltatime);
	}
	if (key == GLFW_KEY_S)
	{
		transform.pitch(-turnSpeed * g_deltatime);
	}
	if (key == GLFW_KEY_A)
	{
		transform.yaw(turnSpeed * g_deltatime);
	}
	if (key == GLFW_KEY_D)
	{
		transform.yaw(-turnSpeed * g_deltatime);
	}
	if (key == GLFW_KEY_Q)
	{
		transform.roll(-turnSpeed * g_deltatime);
	}
	if (key == GLFW_KEY_E)
	{
		transform.roll(turnSpeed * g_deltatime);
	}
	if (key == GLFW_KEY_COMMA)
	{
		speed += minSpeed * 5 * g_deltatime;
		speed = glm::clamp(speed, minSpeed, maxSpeed);
	}
	if (key == GLFW_KEY_PERIOD)
	{
		speed -= minSpeed * 5 * g_deltatime;
		speed = glm::clamp(speed, minSpeed, maxSpeed);
	}
}