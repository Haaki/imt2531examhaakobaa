#include <iostream>
#include "headers/Init.h"
#include "headers/Camera.h"
#include "headers/Functions.h"
#include "headers/Globals.h"
#include "headers/ObjectHandler.h"
#include "headers/logger.h"
#include "headers/Audio.hpp"

#include <SDL.h>
#undef main


GLFWwindow* g_window;

int main()
{
	//	Initializing
	SDL_Init(SDL_INIT_AUDIO);
    initWindow();
	Start();

	//	deltaTime
    double currentframe = glfwGetTime();
    double lastframe = currentframe;
    double lastframe2 = currentframe;

	srand(glfwGetTime());

	LOG_DEBUG("Initialization done! Starting game loop.");
    while (!glfwWindowShouldClose(g_window))
    {
		//	deltaTime
        currentframe = glfwGetTime();
        g_deltatime = currentframe - lastframe;
        double dt2 = currentframe - lastframe2;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//	Update functions
		Update();
		UpdateGameObjects();
        mainCamera->update();
		
        if (dt2 > 1.0)
        {
            LOG_DEBUG("FPS: %f", 1 / g_deltatime);
            lastframe2 = currentframe;
        }

		//	Draw all GameObjects
		DrawGameObjects();
        glfwSwapBuffers(g_window);

        glfwPollEvents();

        lastframe = currentframe;
    }
    glfwTerminate();
	LOG_DEBUG("Exit call recieved, glfw terminated.")
	return 0;
}
