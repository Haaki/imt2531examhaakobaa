#include "src/headers/Terrain.h"
#include "src/headers/Globals.h"
#include "src/headers/HMap.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/logger.h"

Terrain::Terrain()
{

}

void Terrain::update()
{
	if (changingSeasons)
		season += (float)g_deltatime / 4.0f;
	else
		season = season + (seasonGoal - season) * g_deltatime;

	if (season > 5)
		season = 1;
	float s = ((season - 1) / 4.0f)* PI * 2 - PI / 2;
	water->transform.position.y =  terrainScale.y *  (0.1f + glm::cos(s) * 0.001);
	water->transform.scale.y = terrainScale.y * 0.2 + terrainScale.y * glm::cos(s);

	float day = ((daylight - 6) / 4) * PI * 2;
	sun->transform.position = lights[0]->transform.position = glm::vec3(-glm::cos(day)*70.0f, glm::sin(day)*70.0f + 15, 0) * worldScale;
	glm::vec3 c = glm::vec3(0.1f*(glm::sin(day) + 0.25f) - glm::cos(day) / 6, 0.4f*(glm::sin(day) + 0.25f), 0.9f*(glm::sin(day) + 0.25f));
	lights[0]->color = glm::vec3(glm::clamp(glm::sin(day) + 0.5f, 0.0f, 1.0f));
	glClearColor(c.x, c.y, c.z, 1.0f);

	if (changingDaylight)
		daylight += (float)g_deltatime;
	else
		daylight = daylight + (dayGoal - daylight) * g_deltatime * 4;

	if (daylight > 10)
		daylight = 6;

	//Shader, should probably be in draw()...
	for (GameObject* gop : terrainSlices)
	{
		gop->material.specularity = 128;
		gop->material.shader->setInt("hardLines", hardLines);
		gop->material.shader->setFloat("minH", worldScale *  0.0f);
		gop->material.shader->setFloat("maxH", worldScale * (0.6f + glm::cos(s) * 0.3f));
		gop->material.shader->setVec4("vegetationColor", glm::vec4(.3f - glm::cos(s) / 8 + glm::sin(s) / 2, .75f + glm::cos(s) / 8, .2f - glm::sin(s) / 16, 1.0f));
		gop->material.shader->setVec4("mountainColor", glm::vec4(.45f, .45f, .25f, 1.0f));
		gop->material.shader->setVec4("snowColor", glm::vec4(.8f, .9f, 1.0f, 1.0f));
	}

	water->material.specularity = 128;
	water->material.shader->setInt("hardLines", hardLines);
	water->material.shader->setFloat("minH", worldScale *  0.06f);
	water->material.shader->setFloat("maxH", worldScale * (0.7f + glm::cos(s) * 0.3f));
	if (glm::cos(s) + 0.2 > 0) water->material.shader->setVec4("vegetationColor", glm::vec4(0, 0, 1, 1));
	else water->material.shader->setVec4("vegetationColor", glm::vec4(1, 1, 1, 1));
	water->material.shader->setVec4("mountainColor", glm::vec4(0, .45f + glm::cos(s) / 8, .1f - glm::sin(s) / 16, 1.0f));
	water->material.shader->setVec4("snowColor", glm::vec4(.8f, .9f, 1.0f, 1.0f));
	water->material.shader->setVec4("snowColor", glm::vec4(.8f, .9f, 1.0f, 1.0f));

	forest->material.specularity = 128;
	forest->material.shader->setInt("hardLines", hardLines);
	forest->material.shader->setFloat("minH", worldScale *  0.06f);
	forest->material.shader->setFloat("maxH", worldScale * (0.7f + glm::cos(s) * 0.3f));
	forest->material.shader->setVec4("vegetationColor", glm::vec4(0 - glm::cos(s) / 16 + glm::sin(s) / 16, .55f + glm::cos(s) / 8, .2f - glm::sin(s) / 16, 1.0f));
	forest->material.shader->setVec4("mountainColor", glm::vec4(0, .45f + glm::cos(s) / 8, .1f - glm::sin(s) / 16, 1.0f));
	forest->material.specularity = 32;
}

void Terrain::input(int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_O && action == GLFW_RELEASE)
		hardLines = !hardLines;

	if (key == GLFW_KEY_1 && action == GLFW_RELEASE)
	{
		changingSeasons = false;
		seasonGoal = 1;
	}

	if (key == GLFW_KEY_2 && action == GLFW_RELEASE)
	{
		changingSeasons = false;
		seasonGoal = 2;
	}

	if (key == GLFW_KEY_3 && action == GLFW_RELEASE)
	{
		changingSeasons = false;
		seasonGoal = 3;
	}

	if (key == GLFW_KEY_4 && action == GLFW_RELEASE)
	{
		changingSeasons = false;
		seasonGoal = 4;
	}

	if (key == GLFW_KEY_5 && action == GLFW_RELEASE)
	{
		changingSeasons = !changingSeasons;
		seasonGoal = season;
	}

	if (key == GLFW_KEY_6 && action == GLFW_RELEASE)
	{
		changingDaylight = false;
		dayGoal = 6;
	}

	if (key == GLFW_KEY_7 && action == GLFW_RELEASE)
	{
		changingDaylight = false;
		dayGoal = 7;
	}

	if (key == GLFW_KEY_8 && action == GLFW_RELEASE)
	{
		changingDaylight = false;
		dayGoal = 8;
	}

	if (key == GLFW_KEY_9 && action == GLFW_RELEASE)
	{
		changingDaylight = false;
		dayGoal = 9;
	}

	if (key == GLFW_KEY_0 && action == GLFW_RELEASE)
	{
		changingDaylight = !changingDaylight;
		dayGoal = daylight;
	}
}