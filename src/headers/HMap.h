#pragma once

#include <string>
#include "src/headers/Mesh.hpp"



class Hmap 
{
public:
	int height, width, nrChannels;	//	Some misc info about the image
	float amplitude = 255.0f;		//	For adjusting altitude to within 0 and 1

	std::vector<std::vector<GLfloat>> terrainValues;
	std::vector<Mesh> terrainSlices;

	Mesh terrain;
	Mesh water;

	void GenerateHMap(const char* path);
	Mesh GenerateHWater(glm::vec2 size);
	Mesh GenerateHTrees(int amt);

private:
	std::string ReadFile(const char* path);																//	Gets a string of values between 0 and 255 from an image file
	void PopulateValues(std::vector<std::vector<GLfloat>> &values, glm::vec2 size, std::string str);	//	Converts a string of values into a 2d vector of height values
	void PopulateValues(std::vector<std::vector<GLfloat>> &values, glm::vec2 size);						//	Generates a 2d vector of height values

	Mesh MakeTree(glm::vec2 pos);																		//	Creates a tree at pos
	Mesh MakeSlice(std::vector<std::vector<GLfloat>> &values, int collumn);								//	Makes a mesh using a slice of te values
	Mesh JoinMeshes(std::vector<Mesh> meshes, int from, int to);										//	Joins meshes

	void SmoothValues(std::vector<std::vector<GLfloat>> &values);										//	Smoothens the values 
	void Squish(Mesh &values, glm::vec3 size);															//	Squishes the mesh to fit within the size vector, and aligns it to the center
	glm::vec3 VertexNormal(std::vector<std::vector<GLfloat>> &values, glm::vec2 pos);					//	Calculates the nomal of a vertex in values

	std::vector<std::vector<GLfloat>> Noise(glm::vec2 size);
};

