#include "src/headers/Functions.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/Init.h"
#include "src/headers/Globals.h"
#include "src/headers/Camera.h"
#include "src/headers/logger.h"
#include "src/headers/HMap.h"
#include "src/headers/TextBox.h"
#include "src/headers/Glider.h"

#define AUDIO_IMPLEMENTATION
#include "src/headers/Audio.hpp"

TextBox* seasonText;
TextBox* dayText;
TextBox* speedText;
TextBox* camModeText;

Glider* glider;

//--------------------------------------------------------------------
//	USED TO INITIALIZE THE GAME AND YOUR GAMEOBJECTS, CLASSES, AND FUNCTIONS
//--------------------------------------------------------------------
void Start()
{
	LOG_DEBUG("Constructing additional pylons...");
	Mesh* sunMesh = LoadObject("watermelon");
	Mesh* planeMesh = LoadObject("ask21mi");
	Mesh* water = new Mesh;
	Mesh* trees = new Mesh;

	LOG_DEBUG("Painting...");
	Texture* whitetex = LoadTexture("white");
	Texture* waterTex = LoadTexture("blue");
	Texture* sunTex = LoadTexture("yellow");
	Texture* planeTex = LoadTexture("plane");
	Texture* treeTex = LoadTexture("Tree");

	LOG_DEBUG("Acting shady...");
	Shader* standardShader = LoadShader("standard");
	Shader* waterShader = LoadShader("water");
	Shader* textShader = LoadShader("text");
	Shader* heightShader = LoadShader("height");
	Shader* treeShader = LoadShader("treeheight");
	
	Hmap* hmap = new Hmap;

	LOG_DEBUG("Playing minecraft...");
	hmap->GenerateHMap("../resources/textures/height100.png");
	LOG_DEBUG("Filling the lake...");
	hmap->water = hmap->GenerateHWater(glm::vec2(hmap->width / 2, hmap->height / 2));
	LOG_DEBUG("Planting trees...");
	*trees = hmap->GenerateHTrees(100000);

	LOG_DEBUG("Building the world...");
	sun = new GameObject();
	InitGameObject(sun, sunMesh, sunTex, textShader);

	Light* light = CreateLight(glm::vec3(50.0f));

	Terrain* terrain = new Terrain();
	//InitGameObject(terrain, height, whitetex, heightShader);
	InitGameObject(terrain, nullptr, nullptr, nullptr);
	for (Mesh &slice : hmap->terrainSlices)
	{
		GameObject* gop = new GameObject();
		terrain->terrainSlices.push_back(gop);
		InitGameObject(gop, &slice, whitetex, heightShader);
		gop->transform.scale = terrainScale;
	}
	GameObject* lake = new GameObject();
	water = &hmap->water;
	InitGameObject(lake, water, waterTex, waterShader);
	lake->transform.scale = terrainScale;
	terrain->water = lake;

	GameObject* forest = new GameObject();
	InitGameObject(forest, trees, treeTex, treeShader);
	forest->transform.scale = terrainScale;
	terrain->forest = forest;

	glider = new Glider();
	InitGameObject(glider, planeMesh, planeTex, standardShader);
	glider->transform.scale = glm::vec3(0.01f * worldScale);

	mainCamera = new Camera(glm::vec3(6 * worldScale, 13 * worldScale, 0));
	mainCamera->transform.alignWith(terrain->transform.position);
	InitGameObject(mainCamera, nullptr, nullptr, nullptr);
	mainCamera->target = glider;
	mainCamera->followDistance = glm::vec3(0, 0.2f * worldScale, -0.5f * worldScale);

	LOG_DEBUG("Preparing ink...");
	TTF_Font* font = TTF_OpenFont("../resources/fonts/OpenSans-Regular.ttf", 20);
	seasonText = new TextBox(font, "season", { 255, 255, 255, 255 }, glm::vec3(-0.7, 0.35, 0), left, textShader);
	seasonText->transform.scale = glm::vec3(.07f);
	dayText = new TextBox(font, "daylight", { 255, 255, 255, 255 }, glm::vec3(0.7, 0.35, 0), right, textShader);
	dayText->transform.scale = glm::vec3(.07f);
	speedText = new TextBox(font, "speed", { 255, 255, 255, 255 }, glm::vec3(-0.7, -0.30, 0), left, textShader);
	speedText->transform.scale = glm::vec3(.04f);
	camModeText = new TextBox(font, "camMode", { 255, 255, 255, 255 }, glm::vec3(-0.7, -0.35, 0), left, textShader);
	camModeText->transform.scale = glm::vec3(.04f);
}

//--------------------------------------------------------------------
//	USED TO UPDATE THE GAME, OTHERWISE THE SAME AS START
//--------------------------------------------------------------------
void Update()
{
	std::string s = "";
	if (glm::floor(season + .5f) == 1)
		s = "Spring";
	if (glm::floor(season + .5f) == 2)
		s = "Summer";
	if (glm::floor(season + .5f) == 3)
		s = "Autumn";
	if (glm::floor(season + .5f) == 4)
		s = "Winter";
	if (glm::floor(season + .5f) == 5)
		s = "Spring";
	seasonText->text = s;
	seasonText->update();
	seasonText->draw(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f)), mainCamera->UIprojection);

	std::string d = "";
	if (glm::floor(daylight + .5f) == 6)
		d = "Morning";
	if (glm::floor(daylight + .5f) == 7)
		d = "Noon";
	if (glm::floor(daylight + .5f) == 8)
		d = "Evening";
	if (glm::floor(daylight + .5f) == 9)
		d = "Night";
	if (glm::floor(daylight + .5f) == 10)
		d = "Morning";
	dayText->text = d;
	dayText->update();
	dayText->draw(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f)), mainCamera->UIprojection);

	speedText->text = "spd: " + std::to_string(int(glider->speed * 1000));
	speedText->update();
	speedText->draw(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f)), mainCamera->UIprojection);

	std::string c = "";
	if (!mainCamera->planeCam)
		if (mainCamera->cameraMode == 3)
			c =  "Camera mode: World orbit.";
		else
			c = "Camera mode: Free camera.";
	else
		switch (mainCamera->cameraMode)
		{
		case 0: c = "Camera mode: Follow plane.";	break;
		case 1: c = "Camera mode: Cockpit.";		break;
		case 2: c = "Camera mode: Tracking plane."; break;
		case 3: c = "Camera mode: Plane orbit.";	break;
		}
	camModeText->text = c;
	camModeText->update();
	camModeText->draw(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f)), mainCamera->UIprojection);
}
