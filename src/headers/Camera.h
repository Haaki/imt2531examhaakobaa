#pragma once
#include "GameObject.h"
#include "Enums.h"
#include "glm/glm.hpp"

class Camera : public GameObject
{
public:
	bool planeCam = false;
	int cameraMode = 0;

	float mouseSensitivity = 40;
	float zoom = 45;
	float minZoom = 10;
	float maxZoom = 120;

    float rotationAngle = 45.0f;
    float rotationSpeed = 1.0f;

	glm::vec3 rotationDirection = glm::vec3(0, 1, 0);
	glm::vec3 lookatPosition = glm::vec3(0, 0, 0);
	glm::vec3 followDistance = glm::vec3(0, 0, 1);
	glm::vec3 orbitDistance;

    glm::vec3 lookatDirection;

    glm::mat4 projection;
	glm::mat4 UIprojection;
    glm::mat4 view;

    GameObject* target;

    Camera(glm::vec3 pos);

    void update();
    void input(int key, int scancode, int action, int mods);

	void pitch(float angle);
	void yaw(float angle);
	void roll(float angle);

    void updateProjection(int width, int height);
};