#include "src/headers/Globals.h"

int SCREEN_WIDTH = 1920;
int SCREEN_HEIGHT = 1080;

double g_deltatime;

Camera* mainCamera;
Terrain* terrain;
GameObject* sun;

glm::vec2 mousePos;

bool hardLines = false;
float season = 1;
float daylight = 6;
float worldScale = 1;
glm::vec3 terrainScale = glm::vec3(5.04f * worldScale, 0.5f * worldScale, 10.04f * worldScale) * 2.0f;
glm::vec3 home = glm::vec3(-2 * worldScale, 1 * worldScale, 0.7f * worldScale);