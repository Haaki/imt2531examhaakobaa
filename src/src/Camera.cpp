#include "src/headers/Camera.h"
#include "src/headers/Globals.h"
#include "src/headers/Glider.h"
#include "src/headers/logger.h"

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
int lastX = SCREEN_WIDTH / 2.0f;
int lastY = SCREEN_HEIGHT / 2.0f;

Camera::Camera(glm::vec3 pos)
{
	transform.position = pos;
    view = glm::mat4(1.0f);
	orbitDistance = followDistance;
    projection = glm::perspective(glm::radians(zoom), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, 100.0f);
	UIprojection = glm::perspective(glm::radians(45.0f), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, 100.0f);
}

void Camera::update()
{
	if (planeCam)
	{
		switch (cameraMode)
		{
		case 0://follow
			transform.position = target->transform.position + glm::vec3(target->transform.rotation * glm::vec4(followDistance, 1.0f));
			view = glm::lookAt(transform.position, target->transform.position, target->transform.up());
			break;
		case 1://cockpit
			transform.position = target->transform.position;
			view = glm::lookAt(transform.position, target->transform.position + target->transform.forward(), target->transform.up());
			break;
		case 2://tracking
			view = glm::lookAt(transform.position, target->transform.position, glm::vec3(0, 1, 0));
			break;
		case 3://orbiting
			transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed), rotationDirection);
			orbitDistance = transform.rotation * glm::vec4(orbitDistance, 1.0);
			transform.position = target->transform.position + orbitDistance;
			view = glm::lookAt(transform.position, target->transform.position, glm::vec3(0, 1, 0));
			break;
		}
	}
	else
	{
		//freecam
		float x = (mousePos.x - lastX) / SCREEN_WIDTH;
		float y = (mousePos.y - lastY) / SCREEN_HEIGHT;
		pitch(y * g_deltatime * mouseSensitivity);
		yaw(-x * g_deltatime * mouseSensitivity);
		lastX = mousePos.x;
		lastY = mousePos.y;

		view = glm::lookAt(transform.position, transform.position + transform.forward(), glm::vec3(0, 1, 0));

		if (cameraMode == 3)//orbit
		{
			transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed), rotationDirection);
			orbitDistance = transform.rotation * glm::vec4(orbitDistance, 1.0);
			transform.position = lookatPosition + orbitDistance;
			view = glm::lookAt(transform.position, lookatPosition, glm::vec3(0, 1, 0));
		}
			
    }
	//LOG_DEBUG("Camera orientation: x: %f y: %f z: %f", transform.eulerAngles().x, transform.eulerAngles().y, transform.eulerAngles().z);
}

void Camera::input(int key, int scancode, int action, int mods)
{
	if ((key == GLFW_KEY_MINUS || key == GLFW_KEY_KP_SUBTRACT || key == GLFW_KEY_SLASH) && action == GLFW_PRESS)
	{
		planeCam = !planeCam;
	}
    if (key == GLFW_KEY_C && action == GLFW_PRESS)
    {
		cameraMode = (cameraMode + 1) % 4;
		if (!planeCam)
		{
			transform.rotation = glm::mat4(1.0f);
			switch (cameraMode)
			{
			case 0://freecam from world view
				transform.position = glm::vec3(6 * worldScale, 13 * worldScale, 0);
				transform.alignWith( glm::vec3(0) );
				break;
			case 1: //freecam with plane between camera and world origin 
				transform.position = target->transform.position + glm::normalize(target->transform.position) * worldScale;
				transform.alignWith( target->transform.position );
				break;
			case 2:	//freecam from home pos
				transform.position = home;
				transform.alignWith( glm::vec3(0) );
				break;
			case 3:	//orbit around center of the map	
				lookatPosition = glm::vec3(0, 0.5 * worldScale, 0);
				orbitDistance = glm::vec3(6 * worldScale, 7 * worldScale, 0);
			default:
				break;
			}
		}
		else
		{
			orbitDistance = followDistance;
		}
		
		LOG_DEBUG("Cam mode: %d", cameraMode);
    }
	//	Movement of freecam
	//	 Z axis	
    if (key == GLFW_KEY_I) transform.position += transform.forward() * float(worldScale * g_deltatime * 2);
    if (key == GLFW_KEY_K) transform.position -= transform.forward() * float(worldScale * g_deltatime * 2);
	//	X axis 
    if (key == GLFW_KEY_L) transform.position -= transform.right() * float(worldScale * g_deltatime * 2);
    if (key == GLFW_KEY_J) transform.position += transform.right() * float(worldScale * g_deltatime * 2);
	//	Y axis 
    if (key == GLFW_KEY_H) transform.position -= glm::vec3(0, 1, 0) * float(worldScale * g_deltatime * 2);
    if (key == GLFW_KEY_Y) transform.position += glm::vec3(0, 1, 0) * float(worldScale * g_deltatime * 2);

	//	Zoom
	if (key == GLFW_KEY_N)
	{
		zoom -= 20 * g_deltatime;
		zoom = glm::clamp(zoom, minZoom, maxZoom);
		projection = glm::perspective(glm::radians(zoom), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, 100.0f);
	}
	if (key == GLFW_KEY_M)
	{
		zoom += 20 * g_deltatime;
		zoom = glm::clamp(zoom, minZoom, maxZoom);
		projection = glm::perspective(glm::radians(zoom), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, 100.0f);
	}
}

void Camera::pitch(float angle)
{
	float ang = glm::acos(glm::dot(transform.forward(), glm::vec3(0, 1, 0)) / (glm::length(transform.forward()) * glm::length(glm::vec3(0, 1, 0))));
	if (ang < PI / 10 && angle < 0) angle = 0;
	if (ang > PI - (PI / 10) && angle > 0) angle = 0;
	//if (angle != 0)LOG_DEBUG("Cam pitch: %f, angle: %f", ang, angle);
	transform.rotation = glm::toMat4(glm::angleAxis(angle, transform.right()) * glm::quat(transform.eulerAngles()));
}
void Camera::yaw(float angle)
{
	transform.rotation = glm::toMat4(glm::angleAxis(angle, glm::vec3(0, 1, 0)) * glm::quat(transform.eulerAngles()));
}
void Camera::roll(float angle)
{
	transform.rotation = glm::toMat4(glm::angleAxis(angle, transform.forward()) * glm::quat(transform.eulerAngles()));
}

void Camera::updateProjection(int width, int height)
{
    projection = glm::perspective(glm::radians(zoom), float(width) / float(height), 0.1f, 100.f);
	UIprojection = glm::perspective(glm::radians(45.0f), float(width) / float(height), 0.1f, 100.f);
}
