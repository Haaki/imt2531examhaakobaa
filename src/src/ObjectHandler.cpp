#include "src/headers/ObjectHandler.h"
#include "src/headers/mad.h"
#include "src/headers/Init.h"
#include"src/headers/Globals.h"

std::vector<Light*> lights;
std::vector<GameObject*> gameObjects;
std::vector<Mesh*> meshes;
std::vector<Texture*> textures;
std::vector<Shader*> shaders;

Light* CreateLight(glm::vec3 pos, glm::vec3 color)
{
	Light* light = new Light();
	light->transform.position = pos;
	light->color = color;
	lights.push_back(light);

	return light;
}

void InitGameObject(GameObject* go, Mesh* obj, Texture* tex, Shader* shade)
{
	go->init(obj, tex, shade);
}

Mesh* LoadObject(std::string fileName)
{
	std::string path = "../resources/models/";

	Mesh* object = new Mesh(loadObject(path + fileName + ".obj")[0]);
	//objloadertest(object, path + fileName + ".obj");
	meshes.push_back(object);

	return object;
}

Texture* LoadTexture(std::string fileName)
{
	std::string path = "../resources/textures/";

	Texture* texture = new Texture((path + fileName + ".png").c_str());
	textures.push_back(texture);

	return texture;
}

Shader* LoadShader(std::string fileName)
{
	Shader* shader = new Shader(fileName);
	shaders.push_back(shader);

	return shader;
}

void UpdateGameObjects()
{
	for (int i=0; i<gameObjects.size(); i++)
		gameObjects[i]->update();
}

void DrawGameObjects()
{
	for (int i = 0; i<gameObjects.size(); i++)
		gameObjects[i]->draw(mainCamera->view, mainCamera->projection);
}

void HandleInput(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	for (int i = 0; i<gameObjects.size(); i++)
		gameObjects[i]->input(key, scancode, action, mods);
}
