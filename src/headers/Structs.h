#pragma once
#include <vector>

#include "Shaders.h"
#include "Texture.h"

#include <GL/glew.h>
#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>


// Container for pointers to shader and texture, 
// Different materials may contain different combinations of shaders/textures
struct Material
{
    Shader* shader;
    Texture* texture;
	glm::vec4 color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	float specularity = 64;
};
/*
// Container for different mesh attributes
struct Mesh
{
    std::vector<glm::vec3> vertices;                // tinyobj -> attrib_t.vertices
    std::vector<glm::vec2> texture_coordinates;     // tinyobj -> attrib_t.texcoords
    std::vector<glm::uvec3> indices;                // tinyobj -> shape_t.mesh_t.indices.vertex_index
    std::vector<glm::vec3> normals;                 // tinyobj -> attrib_t.normals
};
*/
// Container with functions for position and rotation
struct Transform
{
    glm::vec3 position;
    glm::mat4 rotation;
	glm::vec3 scale;
    float speed;
    void rotate(float angle, glm::vec3 direction, double delta_time)
    {
        rotation = glm::rotate(rotation, glm::radians(angle) * (float) delta_time, direction);
    }
    void translate(glm::vec3 direction, double delta_time)
    {
        position += (direction * (float)delta_time * speed);
    }
	glm::vec3 eulerAngles()
	{
		return glm::eulerAngles(glm::toQuat(rotation));
	}
	glm::vec3 forward()
	{
		return glm::normalize(rotation * glm::vec4(glm::vec3(0, 0, 1), 1.0));
	}
	glm::vec3 up()
	{
		return glm::normalize(rotation * glm::vec4(glm::vec3(0, 1, 0), 1.0));
	}
	glm::vec3 right()
	{
		return glm::normalize(rotation * glm::vec4(glm::vec3(1, 0, 0), 1.0));
	}
	void pitch(float angle)
	{
		rotation = glm::toMat4(glm::angleAxis(angle, right()) * glm::quat(eulerAngles()));
	}
	void yaw(float angle)
	{
		rotation = glm::toMat4(glm::angleAxis(angle, up()) * glm::quat(eulerAngles()));
	}
	void roll(float angle)
	{
		rotation = glm::toMat4(glm::angleAxis(angle, forward()) * glm::quat(eulerAngles()));
	}

	glm::quat RotationBetweenVectors(glm::vec3 start, glm::vec3 dest) {
		start = glm::normalize(start);
		dest = glm::normalize(dest);

		float cosTheta = dot(start, dest);
		glm::vec3 rotationAxis;

		if (cosTheta < -1 + 0.001f) {
			rotationAxis = glm::cross(glm::vec3(0.0f, 0.0f, 1.0f), start);
			if (length2(rotationAxis) < 0.01)
				rotationAxis = glm::cross(glm::vec3(1.0f, 0.0f, 0.0f), start);

			rotationAxis = normalize(rotationAxis);
			return glm::angleAxis(glm::radians(180.0f), rotationAxis);
		}
		rotationAxis = glm::cross(start, dest);

		float s = sqrt((1 + cosTheta) * 2);
		float invs = 1 / s;

		return glm::quat(s * 0.5f, rotationAxis.x * invs, rotationAxis.y * invs, rotationAxis.z * invs);
	}

	glm::quat LookAt(glm::vec3 direction, glm::vec3 desiredUp) {

		if (length2(direction) < 0.0001f)
			return glm::quat();
		glm::vec3 right = glm::cross(direction, desiredUp);
		desiredUp = glm::cross(right, direction);
		glm::quat rot1 = RotationBetweenVectors(glm::vec3(0.0f, 0.0f, 1.0f), direction);
		glm::vec3 newUp = rot1 * glm::vec3(0.0f, 1.0f, 0.0f);
		glm::quat rot2 = RotationBetweenVectors(newUp, desiredUp);

		return rot2 * rot1;
	}

	void alignWith(glm::vec3 target)
	{
		rotation = glm::toMat4(LookAt(target - position, glm::vec3(0, 1, 0)));
	}
};

// Container for different buffer objects
struct Buffer
{
    GLuint VAO;
    GLuint VBO[3];
    GLuint IBO;
};


struct Light
{
	Transform transform;
	glm::vec3 color = glm::vec3(1.0f);
};