#pragma once
#include "src/headers/Camera.h"
#include "src/headers/Terrain.h"

#define PI 3.14159265358979323846264338327950288

extern double g_deltatime;

extern Camera* mainCamera;
extern Terrain* terrain;
extern GameObject* sun;

extern glm::vec2 mousePos;

extern bool hardLines;
extern float season;
extern float daylight;
extern float worldScale;
extern glm::vec3 terrainScale;

extern glm::vec3 home;