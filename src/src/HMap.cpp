#include "src/headers/HMap.h"
#include "src/headers/stb_image.h"
#include "src/headers/logger.h"
#include <thread>  

void Hmap::GenerateHMap(const char* path)	
{
	PopulateValues(terrainValues, glm::vec2(width, height), ReadFile(path));
	SmoothValues(terrainValues);
	std::vector<std::thread> threads;
	for (int i = 1; i < terrainValues.size(); i++)
	{
		terrainSlices.push_back(MakeSlice(terrainValues, i));
	}
	terrain = JoinMeshes(terrainSlices, 0, terrainSlices.size());
	terrainSlices.clear();
	terrainSlices.push_back(terrain);
}
Mesh Hmap::GenerateHWater(glm::vec2 size)
{
	std::vector<std::vector<GLfloat>> waterValues;
	std::vector<Mesh> waterSlices;
	PopulateValues(waterValues, glm::vec2(size.x, size.y));
	amplitude = 255000;
	for (int i = 1; i < waterValues.size(); i++)
		waterSlices.push_back(MakeSlice(waterValues, i));

	amplitude = 255;
	return JoinMeshes(waterSlices, 0, waterSlices.size());
}
Mesh Hmap::GenerateHTrees(int amt)
{
	std::vector<Mesh> trees;
	

	while (trees.size() < amt)
	{
		glm::vec2 root = glm::vec2(rand() % width, rand() % height);
		if (terrainValues[root.x][root.y] > 40)
			trees.push_back(MakeTree(root));
	}
	Mesh forest = JoinMeshes(trees, 0, trees.size() - 1);
	Squish(forest, glm::vec3(width, amplitude, height));
	return forest;	
}

std::string Hmap::ReadFile(const char* path)
{
	unsigned char* data = stbi_load(path, &width, &height, &nrChannels, 0);
	std::string heightString(reinterpret_cast<char*>(data));
	stbi_image_free(data);
	return heightString;
}

void Hmap::PopulateValues(std::vector<std::vector<GLfloat>> &values, glm::vec2 size, std::string str)
{
	GLfloat loVal = 255, hiVal = 0;

	values.resize(size.x + 2);
	values[0].resize(size.y + 2, 0.0f);
	values[size.x + 1].resize(size.y + 2, 0.0f);
	for (int i = 0; i < size.x; i++)
	{
		values[i + 1].push_back(0.0f);
		for (int j = 0; j < size.y; j++)
		{
			GLfloat altitude = 0;
			for (int k = 0; k < nrChannels; k++)
				altitude += str[i * nrChannels  + j * size.x * nrChannels + k]; // NOTE TO SELF: might need to be swapped around if the image is warped
			altitude /= nrChannels;

			loVal = (loVal < altitude) ? loVal : altitude;
			hiVal = (hiVal > altitude) ? hiVal : altitude;

			values[i + 1].push_back(altitude);
		}
		values[i + 1].push_back(0.0f);
	}
	LOG_DEBUG("Populated map with values ranging from %f to %f", loVal, hiVal);
}

void Hmap::PopulateValues(std::vector<std::vector<GLfloat>> &values, glm::vec2 size)
{
	values = Noise(size);
}

Mesh Hmap::MakeTree(glm::vec2 pos)
{
	Mesh tree;
	float height = ((rand() % 1024) / 1204) * 10 + 10;
	float radius = ((rand() % 1024) / 1204) + 1;
	glm::vec2 offset = glm::vec2(((rand() % 1024) / 1204) - 0.5f, ((rand() % 1024) / 1204) - 0.5f);
	//Vertices
	glm::vec3 mVector(pos.x + offset.x, terrainValues[pos.x][pos.y] + height, pos.y + offset.y);
	glm::vec3 tVector(pos.x + offset.x, terrainValues[pos.x][pos.y], pos.y + offset.y - radius);
	glm::vec3 lVector(pos.x + offset.x - radius, terrainValues[pos.x][pos.y], pos.y + offset.y);
	glm::vec3 bVector(pos.x + offset.x, terrainValues[pos.x][pos.y], pos.y + offset.y + radius);
	glm::vec3 rVector(pos.x + offset.x + radius, terrainValues[pos.x][pos.y], pos.y + offset.y);

	tree.vertices.push_back(mVector);	//Middle
	tree.vertices.push_back(tVector);	//Top
	tree.vertices.push_back(lVector);	//Left
	tree.vertices.push_back(mVector);	//Middle
	tree.vertices.push_back(lVector);	//Left
	tree.vertices.push_back(bVector);	//Bottom
	tree.vertices.push_back(mVector);	//Middle
	tree.vertices.push_back(bVector);	//Bottom
	tree.vertices.push_back(rVector);	//Right
	tree.vertices.push_back(mVector);	//Middle
	tree.vertices.push_back(rVector);	//Right
	tree.vertices.push_back(tVector);	//Top

										//Texture coordinates
	glm::vec2 mTex(0.5, 0.5);
	glm::vec2 tTex(0, 0);
	glm::vec2 lTex(0, 1);
	glm::vec2 bTex(1, 1);
	glm::vec2 rTex(1, 0);

	tree.texCoord.push_back(mTex);	//Middle
	tree.texCoord.push_back(tTex);	//Top
	tree.texCoord.push_back(lTex);	//Left
	tree.texCoord.push_back(mTex);	//Middle
	tree.texCoord.push_back(lTex);	//Left
	tree.texCoord.push_back(bTex);	//Bottom
	tree.texCoord.push_back(mTex);	//Middle
	tree.texCoord.push_back(bTex);	//Bottom
	tree.texCoord.push_back(rTex);	//Right
	tree.texCoord.push_back(mTex);	//Middle
	tree.texCoord.push_back(rTex);	//Right
	tree.texCoord.push_back(tTex);	//Top

									//Normals
	glm::vec3 mNormal = glm::normalize(glm::vec3(0, 1, 0));
	glm::vec3 tNormal = glm::normalize(glm::vec3(0, 2, -1));
	glm::vec3 lNormal = glm::normalize(glm::vec3(-1, 2, 1));
	glm::vec3 bNormal = glm::normalize(glm::vec3(0, 2, 1));
	glm::vec3 rNormal = glm::normalize(glm::vec3(0, 2, 1));

	tree.normals.push_back(mNormal);	//Middle
	tree.normals.push_back(tNormal);	//Top
	tree.normals.push_back(lNormal);	//Left
	tree.normals.push_back(mNormal);	//Middle
	tree.normals.push_back(lNormal);	//Left
	tree.normals.push_back(bNormal);	//Bottom
	tree.normals.push_back(mNormal);	//Middle
	tree.normals.push_back(bNormal);	//Bottom
	tree.normals.push_back(rNormal);	//Right
	tree.normals.push_back(mNormal);	//Middle
	tree.normals.push_back(rNormal);	//Right
	tree.normals.push_back(tNormal);	//Top

	return tree;
}

Mesh Hmap::MakeSlice(std::vector<std::vector<GLfloat>> &values, int collumn)
{
	Mesh slice;
	int w = values.size(), h = values[collumn].size();
	for (int i = 1; i < h; i++)
	{
		//	Vertices
		glm::vec3 brVector(collumn, values[collumn][i], i);
		glm::vec3 blVector(collumn, values[collumn][i - 1], i - 1);
		glm::vec3 trVector(collumn - 1, values[collumn - 1][i], i);
		glm::vec3 tlVector(collumn - 1, values[collumn - 1][i - 1], i - 1);

		slice.vertices.push_back(brVector);	//	Bottom Right
		slice.vertices.push_back(tlVector);	//	Top Left
		slice.vertices.push_back(trVector);	//	Top Right
		slice.vertices.push_back(brVector);	//	Bottom Right
		slice.vertices.push_back(tlVector);	//	Top Left
		slice.vertices.push_back(blVector);	//	Bottom Left

		//	Texture coordinates
		glm::vec2 brTex(collumn / w, i / w);
		glm::vec2 blTex((collumn - 1) / w, i / w);
		glm::vec2 trTex(collumn / w, (i - 1) / w);
		glm::vec2 tlTex((collumn - 1) / w, (i - 1) / w);

		slice.texCoord.push_back(brTex);	//	Bottom Right
		slice.texCoord.push_back(tlTex);	//	Top Left
		slice.texCoord.push_back(trTex);	//	Top Right
		slice.texCoord.push_back(brTex);	//	Bottom Right
		slice.texCoord.push_back(tlTex);	//	Top Left
		slice.texCoord.push_back(blTex);	//	Bottom Left

		//	Normals
		glm::vec3 brNorm(VertexNormal(values, glm::vec2(collumn, i)));
		glm::vec3 blNorm(VertexNormal(values, glm::vec2(collumn - 1, i)));
		glm::vec3 trNorm(VertexNormal(values, glm::vec2(collumn, i - 1)));
		glm::vec3 tlNorm(VertexNormal(values, glm::vec2(collumn - 1, i - 1)));

		slice.normals.push_back(brNorm);	//	Bottom Right
		slice.normals.push_back(tlNorm);	//	Top Left
		slice.normals.push_back(trNorm);	//	Top Right
		slice.normals.push_back(brNorm);	//	Bottom Right
		slice.normals.push_back(tlNorm);	//	Top Left
		slice.normals.push_back(blNorm);	//	Bottom Left
	}
	Squish(slice, glm::vec3(values.size(), amplitude, values[collumn].size()));
	return slice;
}

Mesh Hmap::JoinMeshes(std::vector<Mesh> meshes, int from, int to)
{
	Mesh mesh;
	for (Mesh slice : meshes)
	{
		for (glm::vec3 ver : slice.vertices) mesh.vertices.push_back(ver);
		for (glm::vec2 tex : slice.texCoord) mesh.texCoord.push_back(tex);
		for (glm::vec3 norm : slice.normals) mesh.normals.push_back(norm);
	}
	return mesh;
}

void Hmap::SmoothValues(std::vector<std::vector<GLfloat>> &values)
{
	std::vector<std::vector<GLfloat>> oldValues = values;
	for (int i = 1; i < values.size() - 1; i++)
		for (int j = 1; j < values[i].size() - 1; j++)
		{
			GLfloat sum = 0;
			sum += oldValues[i][j] * 2;
			sum += oldValues[i - 1][j] * 2;
			sum += oldValues[i][j - 1] * 2;
			sum += oldValues[i + 1][j] * 2;
			sum += oldValues[i][j + 1] * 2;
			values[i][j] = sum / 6;
		}
}

void Hmap::Squish(Mesh &values, glm::vec3 size)
{
	for (glm::vec3 &v : values.vertices)
	{
		v.x = (v.x - size.x / 2) / size.x;
		v.y = v.y / size.y;
		v.z = (v.z - size.z / 2) / size.z;
	}
	for (glm::vec3 &n : values.normals)
	{
		n.x = n.x / size.x;
		n.y = n.y / size.y;
		n.z = n.z / size.z;
	}
}

glm::vec3 Hmap::VertexNormal(std::vector<std::vector<GLfloat>> &values, glm::vec2 pos)
{
	struct Triangle		//	Used to represet faces
	{
		glm::vec3 A;
		glm::vec3 B;
		glm::vec3 C;
	};
	//	Alignment of the triangles around the point 
	//	o--T	
	//	|\0|\	
	//	|1\|5\	
	//	L--M--R		M represents the point to calculate normals for
	//	 \2|\4|	
	//	  \|3\|	
	//	   B--o	
	glm::vec3 sum(0.0f);	//	The normal of the vector is the sum / average (it's a vector, either works) of the normals of adjacent faces
	int w = values.size(), h = values[pos.x].size();
	Triangle tris[6];	//	All adjacent triangle faces
	for (Triangle &t : tris)  t.A = t.B = t.C = glm::vec3((pos.x, values[pos.x][pos.y], pos.y));	// Middle point, also used if a point is outside the heightmap

	if (pos.x != 0 && pos.y != 0)			//	Top Left
		tris[0].C = tris[1].B = glm::vec3(pos.x - 1, values[pos.x - 1][pos.y - 1], pos.y - 1);
	if (pos.x != 0)							//	Left
		tris[1].C = tris[2].B = glm::vec3(pos.x - 1, values[pos.x - 1][pos.y], pos.y);
	if (pos.y != h - 1)						//	Bottom
		tris[2].C = tris[3].B = glm::vec3(pos.x, values[pos.x][pos.y + 1], pos.y + 1);
	if (pos.x != w - 1 && pos.y != h - 1)	//	Bottom Right 
		tris[3].C = tris[4].B = glm::vec3(pos.x + 1, values[pos.x + 1][pos.y + 1], pos.y + 1);
	if (pos.x != w - 1)						//	Right
		tris[4].C = tris[5].B = glm::vec3(pos.x + 1, values[pos.x + 1][pos.y], pos.y);
	if (pos.y != 0)							//	Top
		tris[5].C = tris[0].B = glm::vec3(pos.x, values[pos.x][pos.y - 1], pos.y - 1);

	//LOG_DEBUG("Triangles around %f, %f", pos.x, pos.y);for (Triangle t : tris)LOG_DEBUG("A: %d.%d, B %d.%d, C %d.%d",(int)t.A.x, (int)t.A.z, (int)t.B.x, (int)t.B.z, (int)t.C.x, (int)t.C.z);

	for (Triangle t : tris) sum += glm::cross(t.B - t.A, t.C - t.A);	//	Sum of all the face normals
	return glm::normalize(sum);
}

//-----------------------
//	Noise Maker			|
//-----------------------
GLfloat NoisePoint(GLfloat maxRand)		//	Creates a random point between 0 and maxRand
{
	return (rand() % 1024) / 1024.0f * maxRand;
}
GLfloat LerpPoint(std::vector<GLfloat> &v, int a, int b, int x)	//	Calculates the blend between a and b at x
{
	return (v[a] * ((b - a) - x) + v[b] * x) / (b - a);
}
std::vector<GLfloat> NoiseLine(int size, int stepSize, GLfloat maxRand)	//	Creates a line of noise
{
	std::vector<GLfloat> line;
	line.resize(size);
	line[0] = NoisePoint(maxRand) * stepSize;
	line[size - 1] = NoisePoint(maxRand) * stepSize;
	for (int i = 0; i < size; i += stepSize)
	{
		if (i + stepSize < size - 1)
		{
			line[i + stepSize] = NoisePoint(maxRand) * stepSize;

			for (int j = 1; j < stepSize; j++)
				line[i + j] = LerpPoint(line, i, i + stepSize, j);
		}
		else
		{
			for (int j = 1; j < size - 1 - i; j++)
				line[i + j] = LerpPoint(line, i,  size - 1 - i, j);
		}
	}
	return line;
}
std::vector<GLfloat> LerpLine(std::vector<GLfloat> A, std::vector<GLfloat> B, int l, int x)	//	Calculates the blend between each of the values in A and B at x, l is the difference in position between A and B
{
	std::vector<GLfloat> line;
	for (int i = 0; i < A.size(); i++)
	{
		std::vector<GLfloat> v;
		v.resize(l + 1);
		v[0] = A[i];
		v[l] = B[i];
		line.push_back(LerpPoint(v, 0, l, x));
	}
	return line;
}
std::vector<std::vector<GLfloat>> NoisePlane(glm::vec2 size, int stepSize, GLfloat maxRand)
{
	std::vector<std::vector<GLfloat>> plane;
	plane.resize(size.x);
	plane[0] = NoiseLine(size.y, stepSize, maxRand);
	plane[size.x - 1] = NoiseLine(size.y, stepSize, maxRand);
	for (int i = 0; i < size.x; i += stepSize)
	{
		if (i + stepSize < size.x - 1)
		{
			plane[i + stepSize] = NoiseLine(size.y, stepSize, maxRand);

			for (int j = 1; j < stepSize; j++)
				plane[i + j] = LerpLine(plane[i], plane[i + stepSize], stepSize, j);
		}
		else
		{
			for (int j = 1; j + i < size.x - 1; j++)
				plane[i + j] = LerpLine(plane[i], plane[size.x - 1 - i], size.x - 1, j);
		}
	}
	return plane;
}
std::vector<std::vector<GLfloat>> Hmap::Noise(glm::vec2 size)
{
	GLfloat loVal = 255, hiVal = 0;

	std::vector<std::vector<GLfloat>> noise;
	noise.resize(size.x);
	for (std::vector<GLfloat> &v : noise)
		v.resize(size.y);

	int hiSize = (size.x > size.y) ? size.x : size.y;
	int stepSize = 1;
	while (stepSize < hiSize)
	{
		std::vector<std::vector<GLfloat>> temp = NoisePlane(size, stepSize, 1.0f);
		for(int i = 0; i < temp.size(); i++)
			for (int j = 0; j < temp[i].size(); j++)
			{
				noise[i][j] += temp[i][j];
			}
		stepSize *= 2;
	}
	for (std::vector<GLfloat> &v : noise)
		for (GLfloat &n : v)
		{
			n /= stepSize;
			loVal = (loVal < n) ? loVal : n;
			hiVal = (hiVal > n) ? hiVal : n;
		}
	LOG_DEBUG("Generated noise, with values ranging from %f to %f", loVal, hiVal);
	return noise;
}
