﻿#pragma once
#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Mesh.hpp"

bool initWindow();
void inputHandler (GLFWwindow* window, int key, int scancode, int action, int mods);
void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos);
void objloadertest(Mesh* mesh, std::string path);

void windowsize_callback(GLFWwindow* window, int width, int height);
